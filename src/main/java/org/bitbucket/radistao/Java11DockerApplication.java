package org.bitbucket.radistao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Java11DockerApplication {

	public static void main(String[] args) {
		SpringApplication.run(Java11DockerApplication.class, args);
	}

}
