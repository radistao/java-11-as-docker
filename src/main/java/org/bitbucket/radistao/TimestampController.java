package org.bitbucket.radistao;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.Instant;

@RestController
public class TimestampController {

    @GetMapping("/timestamp")
    public String timestamp() {
        return Instant.now().toString();
    }
}
