# Java 11 application as lightweight docker image

  - https://stackoverflow.com/questions/53375613/why-is-the-java-11-base-docker-image-so-large-openjdk11-jre-slim

  - https://stackoverflow.com/questions/53669151/java-11-application-as-lightweight-docker-image

1. https://start.spring.io/ with Java 8
    ```
    ./gradlew bootRun
    # or
    # ./gradlew bootJar && java -jar build/libs/application.jar

    curl localhost:8080/timestamp
    ```
2. Add [`Dockerfile`](/Dockerfile)

    - https://spring.io/guides/topicals/spring-boot-docker/
    - https://github.com/docker-library/docs/blob/master/openjdk/README.md#supported-tags-and-respective-dockerfile-links

    - `Dockerfile`
      ```
      FROM openjdk:8-jre-alpine
      COPY build/libs/application.jar app.jar
      ENTRYPOINT ["java","-jar","/app.jar"]
      ```
    - build and run:
      ```
      ./gradlew clean bootJar
      docker build -t java-8-alpine .
      docker images # 102MB
      docker run --rm -p8080:8080 java-8-alpine

      curl localhost:8080/timestamp
      ```

3. Update to Java 11:

  - `gradle.build`:

    ```
    sourceCompatibility = '11'
    targetCompatibility = '11'
    ```
    ```
    ./gradlew clean bootJar
    curl  localhost:8080/timestamp
    ```

  - `Dockerfile`:

    ```
    FROM openjdk:11-jre-alpine
    ```
    ```
    ./gradlew bootJar
    docker build -t java-11-alpine .
    > err: manifest for openjdk:11-jre-alpine not found
    ```

3.1 Use `openjdk:11-jre` base image:

  - `Dockerfile`:

    ```
    FROM openjdk:openjdk:11-jre
    ```
    ```
    ./gradlew bootJar
    docker build -t java-11-debian .
    docker images # 283MB
    docker run --rm -p8080:8080 java-11-debian
    ```

3.2 Use `openjdk:11-jre-slim` base image:

  - `Dockerfile`:

    ```
    FROM openjdk:openjdk:11-jre-slim
    ```
    ```
    ./gradlew bootJar
    docker build -t java-11-slim .
    docker images # 221MB
    ```

3.3 Use `alpine:latest` with `openjdk11-jre-headless`:

  - `Dockerfile`
    ```
    FROM alpine:latest
    
    RUN apk --no-cache add openjdk11-jre-headless
    
    COPY build/libs/application.jar app.jar
    ENTRYPOINT ["java","-jar","/app.jar"]
    ```
    ```
    docker build -t java-11-alpine-jre .
    docker images # 192MB
    docker run --rm -p8080:8080 java-11-alpine-jre
    ```
    
## Java modules (**Java Platform Module System**)
    
  Modularity adds a higher level of aggregation above packages. The key new language element is the module — a uniquely named, reusable group of related packages, as well as resources (such as images and XML files) and a module descriptor specifying.
  
  
  - https://www.oracle.com/corporate/features/understanding-java-9-modules.html
  
  - https://jcp.org/en/jsr/detail?id=376
  
`$ java --list-modules`

`$ java --show-module-resolution -jar build/libs/application.jar`

`$ jdeps --list-deps build/libs/application.jar`

`$ jdeps --print-module-deps build/libs/application.jar`

`$ rm -fr jre/ && jlink \
    --verbose \
    --add-modules java.base,java.logging,java.sql,java.naming,java.desktop,java.management,java.security.jgss,java.instrument \
    --compress 2 --strip-debug --no-header-files --no-man-pages \
    --output jre`
    
`$ jre/bin/java --show-module-resolution  -jar build/libs/application.jar`

### Java 11 Alpine ports and Docker multistage build

  - https://docs.docker.com/develop/develop-images/multistage-build/

#### AdoptOpenJDK

  - https://hub.docker.com/r/adoptopenjdk/openjdk11
  
  - https://blog.gilliard.lol/2018/11/05/alpine-jdk11-images.html
   
  - https://medium.com/azulsystems/using-jlink-to-build-java-runtimes-for-non-modular-applications-9568c5e70ef4

  - https://docs.docker.com/develop/develop-images/multistage-build/

    ```
    docker build --rm -t java-11-alpine-adopt .
    docker images # 84 MB !!
    docker run -p8080:8080 java-11-alpine-adopt
    ```

#### Alpine community JDK
  
  - https://pkgs.alpinelinux.org/package/edge/community/x86_64/openjdk11
  
    ```
    docker build --rm -t java-11-alpine-jmods .
    docker images # 73 MB !!
    docker run -p8080:8080 java-11-alpine-jmods
    ```